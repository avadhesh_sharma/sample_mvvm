//
//  BaseView.swift
//  MVVMDemo
//
//  Created by Avadhesh Sharma on 4/13/18.
//Copyright © 2018 Mobile Programming LLC. All rights reserved.
//

import UIKit
import RxSwift

class BaseView: UIView {
    let disposeBag = DisposeBag()
}

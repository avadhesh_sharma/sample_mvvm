//
//  BaseCollectionViewCell.swift
//  MVVMDemo
//
//  Created by Avadhesh Sharma on 4/13/18.
//Copyright © 2018 Mobile Programming LLC. All rights reserved.
//

import UIKit
import RxSwift

class BaseCollectionViewCell: UICollectionViewCell {
    
    lazy var disposeBag = DisposeBag()
    let onPrepareForReuse: Observable<Void> = PublishSubject()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        (self.onPrepareForReuse as? PublishSubject<Void>)?.on(.next())
    }
    
}

//
//  BaseTableViewCell.swift
//  MVVMDemo
//
//  Created by Avadhesh Sharma on 4/13/18.
//Copyright © 2018 Mobile Programming LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseTableViewCell: UITableViewCell {
    
    lazy var disposeBag = DisposeBag()
    
    let onPrepareForReuse: Observable<Void> = PublishSubject()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        (self.onPrepareForReuse as? PublishSubject<Void>)?.on(.next())
    }
}
